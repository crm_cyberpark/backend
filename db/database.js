const mongoose = require('mongoose');
const dotenv=require('dotenv')
dotenv.config()

mongoose.set('strictQuery', true)

mongoose.connect(process.env.DB_CONNECTION, {
    useNewUrlParser: true,
    useUnifiedTopology:true
}, (err) => {
    if (!err) { console.log('MongoDB Connection Succeeded.') }
    else { console.log('Error in DB connection : ' + err) }
});