const mongoose = require("mongoose");

const courseSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: [true, "Please, write your Group name at least"],
      trim: true,
      min: 4,
    },
    comment: {
      type: String,
      required: [true, 'Please, write your comment at least'],
      trim: true,
      min: 4,
    },
    created_by: {
      type: String,
      required: [true, 'Please, write your created_by at least'],
      trim: true,
      min: 4,
    },
    created_at: {
      type: Date,
      // default: Date.now, // Use the current date and time
    },
    updated_at: {
      type: Date,
      // default: Date.now, // Use the current date and time
    },
  },
  {
    timestamps: false,
    underscored: true,
    toJSON: { getters: true }
    // freezeTableName: true,
  }
);


module.exports = mongoose.model("Course", courseSchema);
