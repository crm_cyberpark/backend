const mongoose = require("mongoose");

const paymentJournalSchema = new mongoose.Schema(
  {
    studentId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Student'
    },
    paymentAmount: {
      type: Number,
      default: 0.00
    },
    paymentType: {
      type: String,
      default:'System'
    },
    courseId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Course'
    },
    groupId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Course'
    },
    created_at: {
      type: Date,
      // default: Date.now, // Use the current date and time
    },
    updated_at: {
      type: Date,
      // default: Date.now, // Use the current date and time
    },
  },
  {
    timestamps: false,
    underscored: true,
    // freezeTableName: true,
  }
);

paymentJournalSchema.index({ studentId: 1 }); // schema level

module.exports = mongoose.model("PaymentJournal", paymentJournalSchema);
