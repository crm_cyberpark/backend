const mongoose = require("mongoose");

const leadSchema = new mongoose.Schema({
  studentId: {
    type: String,
    required: [true, 'Student ID must required!'],
    trim: true
  },
  group: {
    type: String,
    required: [true, 'Group must required!'],
    trim: true
  },
  type: {
    type: String,
    required: [true, 'Type must required!'],
    trim: true
  }
}, { timestamps: true });

leadSchema.index({ phone: 1 }); // schema level

module.exports = mongoose.model("Lead", leadSchema);