const mongoose = require("mongoose");

const paymentSchema = new mongoose.Schema(
  {
    studentId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Student'
    },
    money: {
      type: Number,
      default: 0.00
    },
    type: {
      type: String,
      default:'System'
    },
    adminId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User'
    },
    comment: {
      type: String
    },
    who: {
      type: String
    },
    created_at: {
      type: Date,
      // default: Date.now, // Use the current date and time
    },
    updated_at: {
      type: Date,
      // default: Date.now, // Use the current date and time
    },
  },
  {
    timestamps: false,
    underscored: true,
    // freezeTableName: true,
  }
);

paymentSchema.index({ studentId: 1 }); // schema level

module.exports = mongoose.model("Payment", paymentSchema);
