const mongoose = require("mongoose");

const subscriptionSchema = new mongoose.Schema({
  student_id: { type: mongoose.Schema.Types.ObjectId, ref: 'Student', required: true },
  course_id: { type: mongoose.Schema.Types.ObjectId, ref: 'Course', required: true },
  groupId: { type: mongoose.Schema.Types.ObjectId, ref: 'Group', required: true },
  start_date: { type: Date, required: true },
  end_date: { type: Date, required: true },
  amount:{ type: Number, default: 0.00 },
  comment: { type: String },
  method: { type: String },
  discount: { type: Number, default: 0 },
  status: { type: String, enum: ['active', 'inactive'], required: true, default: 'active' },
  created_at: {
    type: Date,
    // default: Date.now, // Use the current date and time
  },
  updated_at: {
    type: Date,
    // default: Date.now, // Use the current date and time
  },
},
  {
    timestamps: false,
    underscored: true,
    toJSON: { getters: true }
    // freezeTableName: true,
  });

module.exports = mongoose.model('Subscription', subscriptionSchema);
