const mongoose = require("mongoose");

const groupSchema = new mongoose.Schema(
  {
    courseId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Course'
    },
    name: {
      type: String,
    },
    teacherId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Student'
    },
    price: {
      required: true,
      type: Number,
      default: 0.00
    },
    start_date: {
      type: Date,
      required: [true]
    },
    end_date: {
      type: Date,
      required: [true]
    },
    schedule: [{
      type: String,
      // required: [true, 'Please, write your businessCategory at least'],
      trim: true,
    }],
    length: {
      type: String,
      trim: true,
    },
    start_time: {
      type: String,
      required: [true]
    },
    end_time: {
      type: String,
      required: [true]
    },
    status: {
      type: String,
      required: [true]
    },
    type: {
      type: String,
      default: 'general'
    },
    created_at: {
      type: Date,
      // default: Date.now, // Use the current date and time
    },
    updated_at: {
      type: Date,
      // default: Date.now, // Use the current date and time
    },
  },
  {
    timestamps: false,
    underscored: true,
    // freezeTableName: true,
  }
);

groupSchema.index({ pinfl: 1 }); // schema level

module.exports = mongoose.model("Group", groupSchema);
