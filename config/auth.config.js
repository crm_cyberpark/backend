module.exports = {
    secret: "cyberpark-secret-key",
    jwtExpiration: 3600*10,           // 1 hour *10
    jwtRefreshExpiration: 86400,   // 24 hours
  
    /* for test */
    // jwtExpiration: 60,          // 1 minute
    // jwtRefreshExpiration: 120,  // 2 minutes
  };