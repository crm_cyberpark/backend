require('./db/database')
require('module-alias/register');
const express = require('express')
const app = express()
const dotenv = require('dotenv')
const cors = require('cors')
const morgan = require('morgan')
const path = require('path');
const basicAuth = require('express-basic-auth');
const bodyParser = require('body-parser')
const swaggerJsDoc = require('swagger-jsdoc');
const swaggerUI = require('swagger-ui-express');
const router = require('@/api/router/index')
// require('@/api/services/subscriptionProcessor')
dotenv.config()

const PORT = process.env.PORT || 9000;
const HOST = process.env.HOST || '127.0.0.1';

const corsOptions = {
  origin:
    ['*'],
  credentials: true,
  optionSuccessStatus: 200
}

app.use(morgan('dev'))
app.use(cors())
app.use(bodyParser.json({ limit: '10mb', extended: true }));
app.use(bodyParser.urlencoded({ limit: '10mb', extended: true }));

const swaggerOptions = {
  swaggerDefinition: {
    info: {
      title: "Cyber science API",
      version: '1.0.0',
    },
  },
  apis: ["app.js", "./router/*.js"],
  securityDefinitions: {
    auth: {
      type: 'basic'
    }
  }
};
const swaggerDocs = swaggerJsDoc(swaggerOptions);
app.use('/api/v1/api-docs', basicAuth({
  users: { 'admin': '8723' },
  challenge: true,
}), swaggerUI.serve, swaggerUI.setup(swaggerDocs));

/**
 * @swagger
 * /api/v1/status:
 *   get:
 *     description: Health Check!
 *     tags:
 *       - Check server
 *     responses:
 *       200:
 *         description: Success
 * 
 */
app.use('/status', (req, res) => {
  res.json({ Hello: "World!" })
})

app.use('/api/v1', router)
app.use('/api/v1/uploads', express.static(path.join(__dirname, 'uploads')));

app.listen(PORT, (err) => {
  if (err) { console.log(`Error:${err}`) }
  console.log(`Running on port http://${HOST}:${PORT}/api/v1/api-docs, SUCCESSFULLY!`)
})
