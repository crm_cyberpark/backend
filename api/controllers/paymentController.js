const Student = require("@models/student");
const User = require("@models/user");
const dotenv = require('dotenv');
const { userLogger } = require("@/api/helpers/logger");
const currency = require("currency.js");
const Payment = require("@/db/models/payment");
const getCurrentDateTime = require("@/api/helpers/time");
const Subscription = require("@/db/models/subscription");
const moment = require('moment');
const group = require("@/db/models/group");
// const { enrollStudentToSubscriptionPaymenttest } = require("../services/subscriptionService");
const Course = require("@/db/models/course");
const group_list = require("@/db/models/group_list");
const { default: mongoose } = require("mongoose");

dotenv.config()

async function transferMoney(studentId, money, adminId, type, comment = '', who) {
  try {
    // jo'natuvchini bazadan izlab topamiz, bunda bazaga so'rov berilyapti, shuning uchun
    // tranzaktsiya ichida bajariladigan barcha so'rovlarga sessiyani ham
    // berib yuborishimiz kerak
    const student = await Student.findOne({ _id: studentId })
    if (!student)
      throw new Error('Sender not found');

    const admin = await User.findOne({ _id: adminId })
    if (!admin)
      throw new Error('Admin not found');

    // var regex = /^\d+(?:\.\d{0,2})$/;
    // var numStr = "123.20";
    // if (regex.test(money))
    //   throw new Error('Money is not valid');
    // jo'natuvchining hisobidan tranzaktsiya miqdoricha pulni ayrib tashlaymiz
    student.balance = currency(student.balance).add(money);
    // jo'natuvchiga qilingan o'zgarishlarni bazaga yozib qo'yamiz
    // bu yerda sessiya obyektini berishni keragi yo'q.
    await student.save();

    // throw new Error('Bu allaqanday bir xato!!! Ajjab bo\'lsin!');
    // jo'natuvchining hisobidan pul yechib olingan amal haqida
    // jurnal yozamiz
    let debitJournal = new Payment({
      studentId: studentId,
      money: money,
      adminId: adminId,
      type: type,
      comment: comment,
      who: who,
      created_at: getCurrentDateTime(),
      updated_at: getCurrentDateTime()
    })
    await debitJournal.save();
    console.log('Transaction has been completed successfully!');
  } catch (error) {
    console.error(error);
    throw error;
  }
}
// Enroll student to subscription scheme
async function enrollStudentToSubscriptionPayment(studentId, courseId, groupId, amount, start_date, end_date, discount, method, comment) {
  try {

    const course = await Course.findById(courseId);
    if (!course) {
      throw new Error('Course not found.');
    }

    // const subscription = await Subscription.find({
    //     student_id: studentId,
    //     course_id: courseId,
    //     groupId: groupId
    // });

    // console.log('!subscription');
    // console.log(subscription);
    // if (!subscription) {
    //     throw new Error('subscription already exist.');
    // }

    const today = moment().startOf('day').toDate();

    // Create new subscription
    const newSubscription = new Subscription({
      student_id: studentId,
      course_id: courseId,
      groupId: groupId,
      start_date: start_date,
      end_date: end_date,
      discount: discount,
      amount: amount,
      status: 'active',
      method: method,
      comment: comment,
      created_at: getCurrentDateTime(),
      updated_at: getCurrentDateTime()
    });

    await newSubscription.save();
    // await notifyStudent(studentId, `You have been enrolled in the course and your initial subscription is valid until ${moment(today).format('YYYY-MM-DD')}.`);
  } catch (error) {
    console.log(error)
    throw new Error('error enrollStudentToSubscription');
  }
}

exports.list = async (req, res) => {
  // Our login logic starts here
  try {
    // Get user input
    var { pageNumber, pageSize, studentID = '' } = req.query;
    pageNumber = parseInt(pageNumber);
    pageSize = parseInt(pageSize);
    // Validate user input
    if (!(pageNumber && pageSize)) {
      return res.status(400).send("Required iputs is missed!");
    }
    let query = {amount: { $gt: 0 }};

    if (studentID != '') {
      query.student_id = studentID;
    }
    // Validate if user exist in our database
    var payment = await Subscription.find(query)
      .populate({
        path: 'student_id',
        select: '_id first_name middle_name last_name'
      })
      .populate({
        path: 'course_id',
        select: '-_id name'
      })
      .populate({
        path: 'groupId',
        select: '-_id name'
      })
      .sort([['updated_at', -1]])
      .skip((pageNumber - 1) * pageSize)
      .limit(pageSize)
      .exec();

    const length = await Subscription.count(query)
      .exec();
    const total_page = Math.floor(length / pageSize) + 1
    if (payment.err || payment <= 0) {
      return res
        .status(404)
        .json({
          code: 404,
          message: "There as not any payment yet"
        });
    } else {
      return res
        .status(200)
        .json({ code: 200, message: "payment exist", total_page: total_page, payments: payment });
    }
  } catch (err) {
    userLogger.error(err);
    console.log(err);
    return res.status(500).json({ code: 500, message: 'Internal server error', error: err });
  }
  // Our register logic ends here
}

exports.subs_list = async (req, res) => {
  // Our login logic starts here
  try {
    // Get user input
    var { pageNumber, pageSize } = req.query;
    pageNumber = parseInt(pageNumber);
    pageSize = parseInt(pageSize);
    // Validate user input
    if (!(pageNumber && pageSize)) {
      return res.status(400).send("Required iputs is missed!");
    }
    // Validate if user exist in our database
    var subscription = await Subscription.find({})
      .sort([['updated_at', -1]])
      .skip((pageNumber - 1) * pageSize)
      .limit(pageSize)
      .exec();
    const total_page = Math.floor(subscription.length / pageSize) + 1
    if (subscription.err || subscription <= 0) {
      return res
        .status(404)
        .json({
          code: 404,
          message: "There as not any Subscription yet"
        });
    } else {
      return res
        .status(200)
        .json({ code: 200, message: "Subscription exist", total_page: total_page, subscription: subscription });
    }
  } catch (err) {
    userLogger.error(err);
    console.log(err);
    return res.status(500).json({ code: 500, message: 'Internal server error', error: err });
  }
  // Our register logic ends here
}

exports.payment = async (req, res) => {

  // Our register logic starts here
  try {

    // Get user input
    const { studentId, money, adminId, type, comment, groupId, startDate, endDate, discount, method = type } = req.body;
    // Validate user input
    if (!(studentId && money && adminId && type)) {
      return res.status(400).json({ code: 400, message: 'Required iputs is missed!' });
    }

    const course = await group.findById(groupId);
    
    if (!course) {
      return res.status(400).json({ code: 400, message: 'Course not found' });
      console.log(`Course for subscription is not available`);
    }

    const value = {
      studentId: studentId,
      groupId: groupId
    };
    //check student already in this group
    // const check = await group_list(value);
    // if (check) {
    //   return res.status(400).json({ code: 400, message: 'Already is added to group' });
    // }

    const check = await group_list.find(value)

    if (check.length == 0) {
      return res.status(409).json({ code: 409, message: 'students is not in the group' });
    }
    // await transferMoney(studentId, money, adminId, type, comment, 'byAdmin')

    // console.log("nimadir")
    // const success = await enrollStudentToSubscriptionPaymenttest('Nimadir')
    // await transferMoney(studentId, money, adminId, type, comment, 'byAdmin')
    await enrollStudentToSubscriptionPayment(studentId, course?.courseId, groupId, money, startDate, endDate, discount, method, comment)

    return res.status(201).json('success');
  } catch (err) {
    console.log(err)
    return res.status(500).json({ code: 500, message: 'Internal server error', error: err });
  }
  // Our register logic ends here
};

exports.getById = async (req, res) => {

  // Our login logic starts here
  try {
    // Get user input
    var { id } = req.query;
    // Validate if user exist in our database
    var student = await Payment.find({ _id: id })
      .populate({
        path: 'studentId',
        select: '-_id first_name'
      })
      .populate({
        path: 'adminId',
        select: '-_id first_name'
      })
      .sort([['updated_at', -1]])
      .exec();

    if (student.err || student <= 0) {
      return res
        .status(404)
        .json({
          code: 404,
          message: "There as not any student yet"
        });
    } else {
      return res
        .status(200)
        .json({ code: 200, message: "student exist", student: student });
    }
  } catch (err) {
    userLogger.error(err);
    console.log(err);
    return res.status(500).json({ code: 500, message: 'Internal server error', error: err });
  }
  // Our register logic ends here
}

exports.deleteById = async (req, res) => {

  // Our login logic starts here
  try {
    // Get user input
    var { id } = req.query;
    // Validate if user exist in our database
    var payment = await Subscription.findByIdAndDelete(id)
      .exec();

    if (payment == null) {
      return res
        .status(404)
        .json({
          code: 404,
          message: "There as not any payment yet"
        });
    } else {
      return res
        .status(200)
        .json({ code: 200, message: "payment exist and deleted" });
    }
  } catch (err) {
    userLogger.error(err);
    console.log(err);
    return res.status(500).json({ code: 500, message: 'Internal server error', error: err });
  }
  // Our register logic ends here
}

exports.deleteById_subs = async (req, res) => {

  // Our login logic starts here
  try {
    // Get user input
    var { id } = req.query;
    // Validate if user exist in our database
    var payment = await Subscription.deleteMany()
      .exec();

    if (payment == null) {
      return res
        .status(404)
        .json({
          code: 404,
          message: "There as not any payment yet"
        });
    } else {
      return res
        .status(200)
        .json({ code: 200, message: "payment exist and deleted" });
    }
  } catch (err) {
    userLogger.error(err);
    console.log(err);
    return res.status(500).json({ code: 500, message: 'Internal server error', error: err });
  }
  // Our register logic ends here
}

exports.calcuMnthCrs = async (req, res) => {

  // Our login logic starts here
  try {
    const startOfMonth = moment.utc().startOf('month').toDate();
    const endOfMonth = moment.utc().endOf('month').toDate();
    // Validate if user exist in our database
    var payment = await Payment.find({
      created_at: {
        $gte: startOfMonth,
        $lt: endOfMonth
      },
      who: "byAdmin"
    });
    var money = 0
    for (let pyt of payment) {
      money = currency(pyt.money * (-1)).add(money);
    }
    if (payment == null) {
      return res
        .status(404)
        .json({
          code: 404,
          message: "There as not any payment yet"
        });
    } else {
      return res
        .status(200)
        .json({ code: 200, message: "payment exist", money: money });
    }
  } catch (err) {
    userLogger.error(err);
    console.log(err);
    return res.status(500).json({ code: 500, message: 'Internal server error', error: err });
  }
  // Our register logic ends here
}

exports.calcuMnthCrsSystem = async (req, res) => {
  // Our login logic starts here
  try {
    const startOfMonth = moment.utc().startOf('month').toDate();
    const endOfMonth = moment.utc().endOf('month').toDate();
    // Validate if user exist in our database
    var payment = await Payment.find({
      created_at: {
        $gte: startOfMonth,
        $lt: endOfMonth
      },
      who: "bySystem"
    });
    var money = 0
    for (let pyt of payment) {
      money = currency(pyt.money * (-1)).add(money);
    }
    if (payment == null) {
      return res
        .status(404)
        .json({
          code: 404,
          message: "There as not any payment yet"
        });
    } else {
      return res
        .status(200)
        .json({ code: 200, message: "payment exist", money: money });
    }
  } catch (err) {
    userLogger.error(err);
    console.log(err);
    return res.status(500).json({ code: 500, message: 'Internal server error', error: err });
  }
  // Our register logic ends here
}

exports.calcGroupMoney = async (req, res) => {
  const { groupId } = req.body;
  // Our login logic starts here
  try {
    // Validate if user exist in our database
    var payment = await Subscription.aggregate([
      {
        $match: { groupId: mongoose.Types.ObjectId(groupId) } // Match documents with the given groupId
      },
      {
        $group: {
          _id: null, // We don't need to group by any specific field, so set _id to null
          totalAmount: { $sum: "$amount" } // Sum the 'amount' field
        }
      }
    ]);
    return res
      .status(200)
      .json({ code: 200, message: "Sums exist", money: payment });
  } catch (err) {
    userLogger.error(err);
    console.log(err);
    return res.status(500).json({ code: 500, message: 'Internal server error', error: err });
  }
  // Our register logic ends here
}

module.exports.transferMoney = transferMoney;