const config = require("@/config/auth.config");
const Leads = require("@models/lead");

exports.add = async (req, res) => {
  // Our register logic starts here
  try {
    // Logic
    const { phone, first_name, last_name, father_name, birth_date, note, group, type, role } = req.body;
    // Validate user input
    if (!(phone && first_name && birth_date)) {
      return res.status(400).json({ code: 400, message: 'All input is required' });
    }
    
    const oldUser = await Student.findOne({ phone });

    if (oldUser) {
      return res.status(400).json({ code: 400, message: 'This phone number already registered' });
    }
    
    const value = {
      phone: phone,
      first_name: first_name,
      last_name: last_name,
      father_name: father_name,
      birth_date: birth_date,
      note: note,
      role: role || 'lead',
    };
    const user = new Student(value);
    // validation
    var error = user.validateSync();
    if (error) {
      return res.status(409).json({ code: 409, message: 'Validatioan error', error: error });
      // return res.status(409).send("Validatioan error");
    }
    const saved_user = await user.save();
    // if (name && url) {
    //   user.img = { name: name, url: url }
    // }
    // return new user
    const valueLeads = {
      studentId:saved_user._id,
      group:group,
      type:type
    }
    console.log(valueLeads)
    const lead = new Leads(valueLeads);
    var error = lead.validateSync();
    if (error) {
      return res.status(409).json({ code: 409, message: 'Validatioan error', error: error });
      // return res.status(409).send("Validatioan error");
    }
    const saved_lead = await lead.save();
    return res.status(201).json(saved_lead);
  } catch (err) {
    console.log(err)
    return res.status(500).json({ code: 500, message: 'Internal server error', error: error });
  }
};

exports.get_list = async (req, res) => {

  // Our login logic starts here
  try {
    // Get user input
    var { pageNumber, pageSize } = req.query;
    pageNumber = parseInt(pageNumber);
    pageSize = parseInt(pageSize);
    // Validate user input
    if (!(pageNumber && pageSize)) {
      return res.status(400).send("Required inputs is missed!");
    }
    // Validate if user exist in our database
    var leads = await Leads.find({})
      .sort([['updatedAt', -1]])
      .skip((pageNumber - 1) * pageSize)
      .limit(pageSize)
      .exec();

    const total_page=Math.floor(leads.length/pageSize)+1

      if (leads.err || leads <= 0) {
        return res
          .status(404)
          .json({
            code: 404,
            message: "There as not any leads yet"
          });
      } else {
        return res
          .status(200)
          .json({ code: 200, message: "leads exist", total_page:total_page, leads: leads });
      }
  } catch (err) {
    userLogger.error(err);
    console.log(err);
    return res.status(500).json({ code: 500, message: 'Internal server error', error: err });
  }
  // Our register logic ends here
}

exports.getById = async (req, res) => {

  // Our login logic starts here
  try {
    // Get user input
    var { id } = req.query;
    // Validate if user exist in our database
    var lead = await Leads.find({_id:id})
      .sort([['updatedAt', -1]])
      .exec();

      if (lead.err || lead <= 0) {
        return res
          .status(404)
          .json({
            code: 404,
            message: "There as not any lead yet"
          });
      } else {
        return res
          .status(200)
          .json({ code: 200, message: "lead exist", lead: lead });
      }
  } catch (err) {
    userLogger.error(err);
    console.log(err);
    return res.status(500).json({ code: 500, message: 'Internal server error', error: err });
  }
  // Our register logic ends here
}

exports.deleteById = async (req, res) => {

  // Our login logic starts here
  try {
    // Get user input
    var { id } = req.query;
    // Validate if user exist in our database
    var student = await Leads.findOneAndDelete({_id:id})
      .exec();
      console.log(student)
      if (student==null) {
        return res
          .status(404)
          .json({
            code: 404,
            message: "There as not any lead yet"
          });
      } else {
        return res
          .status(200)
          .json({ code: 200, message: "lead exist and deleted"});
      }
  } catch (err) {
    userLogger.error(err);
    console.log(err);
    return res.status(500).json({ code: 500, message: 'Internal server error', error: err });
  }
  // Our register logic ends here
}

//   const { refreshToken: requestToken } = req.body;

//   if (requestToken == null) {
//     return res.status(403).json({ message: "Refresh Token is required!" });
//   }

//   try {
//     let refreshToken = await RefreshToken.findOne({ token: requestToken });

//     if (!refreshToken) {
//       res.status(403).json({ message: "Refresh token is not in database!" });
//       return;
//     }

//     if (RefreshToken.verifyExpiration(refreshToken)) {
//       RefreshToken.findByIdAndRemove(refreshToken._id, { useFindAndModify: false }).exec();
      
//       res.status(403).json({
//         message: "Refresh token was expired. Please make a new signin request",
//       });
//       return;
//     }

//     let newAccessToken = jwt.sign({ id: refreshToken.user._id }, config.secret, {
//       expiresIn: config.jwtExpiration,
//     });

//     return res.status(200).json({
//       accessToken: newAccessToken,
//       refreshToken: refreshToken.token,
//     });
//   } catch (err) {
//     return res.status(500).send({ message: err });
//   }
// };