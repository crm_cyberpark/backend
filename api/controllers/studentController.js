const config = require("@/config/auth.config");
const Student = require("@models/student");
const RefreshToken = require("@models/refreshToken.model");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");
const dotenv = require('dotenv');
const { userLogger } = require("../helpers/logger");

dotenv.config()

exports.signup = async (req, res) => {

  // Our register logic starts here
  try {
    // Get user input
    const { phone, first_name, last_name, middle_name, birth_date, gender, note, password, role } = req.body;
    // Validate user input
    if (!(phone && first_name && gender)) {
      return res.status(400).json({ code: 400, message: 'Required iputs is missed!' });
    }

    // check if user already exist
    // Validate if user exist in our database
    const oldUser = await Student.findOne({ phone });

    if (oldUser) {
      return res.status(400).json({ code: 400, message: 'User Already Exist. Please Login' });
      // return res.status(409).send("User Already Exist. Please Login");
    }
    
    const value = {
      phone: phone,
      first_name: first_name,
      last_name: last_name,
      middle_name: middle_name,
      birth_date: birth_date,
      gender: gender,
      note: note,
      status: 'active',
      password: password,
      role: role || 'student',
    };
    //Encrypt user password
    if (password) {
      encryptedPassword = await bcrypt.hash(password, 10);
      value.password=encryptedPassword
    }
    const user = new Student(value);
    // validation
    var error = user.validateSync();
    if (error) {
      return res.status(409).json({ code: 409, message: 'Validatioan error', error: error });
      // return res.status(409).send("Validatioan error");
    }
    const saved_user = await user.save();
    // if (name && url) {
    //   user.img = { name: name, url: url }
    // }
    // return new user
    return res.status(201).json(saved_user);
  } catch (err) {
    console.log(err)
    return res.status(500).json({ code: 500, message: 'Internal server error', error: error });
  }
  // Our register logic ends here
};

exports.signin = async (req, res) => {

  // Our login logic starts here
  try {
    // Get user input
    const { phone, password } = req.body;

    // Validate user input
    if (!(phone && password)) {
      return res.status(400).send("Required iputs is missed!");
    }
    // Validate if user exist in our database
    const user = await Student.findOne({ phone: phone, status: 'active' });

    if (user && (await bcrypt.compare(password, user.password))) {
      // Create token
      const token = jwt.sign({ id: user._id }, config.secret, {
        expiresIn: config.jwtExpiration,
      });
      let refreshToken = await RefreshToken.createToken(user);
      let authorities = [];
      for (let i = 0; i < user.role.length; i++) {
        authorities.push("ROLE_" + user.role[i].toUpperCase());
      }
      // save user token
      user.token = token;
      user.refreshToken = refreshToken;
      user.authorities = authorities;
      // user
      return res.status(200).json({ data: user, token: token, refreshToken: refreshToken, authorities: authorities });
    }
    return res.status(400).json({ code: 400, message: 'user does not exist and not verified' });
  } catch (err) {
    userLogger.error(err);
    console.log(err);
    return res.status(500).json({ code: 500, message: 'Internal server error', error: err });
  }
  // Our register logic ends here
}

exports.list = async (req, res) => {
  // Our login logic starts here
  try {
    // Get user input
    var { pageNumber, pageSize, query } = req.query;
    pageNumber = parseInt(pageNumber);
    pageSize = parseInt(pageSize);
    // Validate user input
    if (!(pageNumber && pageSize)) {
      return res.status(400).send("Required iputs is missed!");
    }
    // Validate if user exist in our database
    var student = await Student.find({
        role: 'student',
        status: 'active',
        $or: [
          { first_name: new RegExp(query, 'i') },
          { last_name: new RegExp(query, 'i') }
        ]
      })
      .sort([['updatedAt', -1]])
      .skip((pageNumber - 1) * pageSize)
      .limit(pageSize)
      .exec();
    const length = await Student.count({
        role: 'student',
        status: 'active',
        $or: [
          { first_name: new RegExp(query, 'i') },
          { last_name: new RegExp(query, 'i') }
        ]
      })
      .exec();
    const total_page=Math.floor(length/pageSize)+1
      if (student.err || student <= 0) {
        return res
          .status(404)
          .json({
            code: 404,
            message: "There as not any student yet"
          });
      } else {
        return res
          .status(200)
          .json({ code: 200, message: "student exist",total_page:total_page, student: student });
      }
  } catch (err) {
    userLogger.error(err);
    console.log(err);
    return res.status(500).json({ code: 500, message: 'Internal server error', error: err });
  }
  // Our register logic ends here
}

exports.getById = async (req, res) => {

  // Our login logic starts here
  try {
    // Get user input
    var { id } = req.query;
    // Validate if user exist in our database
    var student = await Student.find({_id:id, status: 'active'})
      .sort([['updatedAt', -1]])
      .exec();

      if (student.err || student <= 0) {
        return res
          .status(404)
          .json({
            code: 404,
            message: "There as not any student yet"
          });
      } else {
        return res
          .status(200)
          .json({ code: 200, message: "student exist", student: student });
      }
  } catch (err) {
    userLogger.error(err);
    console.log(err);
    return res.status(500).json({ code: 500, message: 'Internal server error', error: err });
  }
  // Our register logic ends here
}

exports.getByGroup = async (req, res) => {

  // Our login logic starts here
  try {
    // Get user input
    var { group } = req.query;
    // Validate if user exist in our database
    var student = await Student.find({_id:id, status: 'active'})
      .sort([['updatedAt', -1]])
      .exec();

      if (student.err || student <= 0) {
        return res
          .status(404)
          .json({
            code: 404,
            message: "There as not any student yet"
          });
      } else {
        return res
          .status(200)
          .json({ code: 200, message: "student exist", student: student });
      }
  } catch (err) {
    userLogger.error(err);
    console.log(err);
    return res.status(500).json({ code: 500, message: 'Internal server error', error: err });
  }
  // Our register logic ends here
}

exports.deleteById = async (req, res) => {

  // Our login logic starts here
  try {
    // Get user input
    var { id } = req.query;
    // Validate if user exist in our database
    var student = await Student.findOneAndDelete({_id:id})
      .exec();

      if (student==null) {
        return res
          .status(404)
          .json({
            code: 404,
            message: "There as not any student yet"
          });
      } else {
        return res
          .status(200)
          .json({ code: 200, message: "student exist and deleted"});
      }
  } catch (err) {
    userLogger.error(err);
    console.log(err);
    return res.status(500).json({ code: 500, message: 'Internal server error', error: err });
  }
  // Our register logic ends here
}

//this is test now
exports.listArchive = async (req, res) => {
  // Our login logic starts here
  try {
    // Get user input
    var { pageNumber, pageSize, query } = req.query;
    pageNumber = parseInt(pageNumber);
    pageSize = parseInt(pageSize);
    // Validate user input
    if (!(pageNumber && pageSize)) {
      return res.status(400).send("Required iputs is missed!");
    }
    // Validate if user exist in our database
    var student = await Student.find({
        role: 'student',
        status: 'inactive',
        $or: [
          { first_name: new RegExp(query, 'i') },
          { last_name: new RegExp(query, 'i') }
        ]
      })
      .sort([['updatedAt', -1]])
      .skip((pageNumber - 1) * pageSize)
      .limit(pageSize)
      .exec();
    const length = await Student.count({
        role: 'student',
        status: 'inactive',
        $or: [
          { first_name: new RegExp(query, 'i') },
          { last_name: new RegExp(query, 'i') }
        ]
      })
      .exec();
    const total_page=Math.floor(length/pageSize)+1
      if (student.err || student <= 0) {
        return res
          .status(404)
          .json({
            code: 404,
            message: "There as not any student yet"
          });
      } else {
        return res
          .status(200)
          .json({ code: 200, message: "student exist",total_page:total_page, student: student });
      }
  } catch (err) {
    userLogger.error(err);
    console.log(err);
    return res.status(500).json({ code: 500, message: 'Internal server error', error: err });
  }
  // Our register logic ends here
}

exports.statusChange = async (req, res) => {
  // Our login logic starts here
  try {
    // Get user input
    const { id } = req.query;
    // Validate if user exist in our database
    const student = await Student.findOneAndUpdate({_id:id},{status: 'inactive'}).exec();

    if (student==null) {
      return res
        .status(404)
        .json({
          code: 404,
          message: "There as not any student yet"
        });
    } else {
      return res
        .status(200)
        .json({ code: 200, message: "student exist and deleted"});
    }
  } catch (err) {
    userLogger.error(err);
    console.log(err);
    return res.status(500).json({ code: 500, message: 'Internal server error', error: err });
  }
  // Our register logic ends here
}
// exports.refreshToken = async (req, res) => {
//   const { refreshToken: requestToken } = req.body;

//   if (requestToken == null) {
//     return res.status(403).json({ message: "Refresh Token is required!" });
//   }

//   try {
//     let refreshToken = await RefreshToken.findOne({ token: requestToken });

//     if (!refreshToken) {
//       res.status(403).json({ message: "Refresh token is not in database!" });
//       return;
//     }

//     if (RefreshToken.verifyExpiration(refreshToken)) {
//       RefreshToken.findByIdAndRemove(refreshToken._id, { useFindAndModify: false }).exec();
      
//       res.status(403).json({
//         message: "Refresh token was expired. Please make a new signin request",
//       });
//       return;
//     }

//     let newAccessToken = jwt.sign({ id: refreshToken.user._id }, config.secret, {
//       expiresIn: config.jwtExpiration,
//     });

//     return res.status(200).json({
//       accessToken: newAccessToken,
//       refreshToken: refreshToken.token,
//     });
//   } catch (err) {
//     return res.status(500).send({ message: err });
//   }
// };