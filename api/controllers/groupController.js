const Group = require("@/db/models/group");
const dotenv = require('dotenv');
const getCurrentDateTime = require("@/api/helpers/time");
const { userLogger } = require("@/api/helpers/logger");
const { default: mongoose } = require("mongoose");
const group_list = require("@/db/models/group_list");
const { enrollStudentToSubscription } = require("@/api/services/subscriptionService");
const Subscription = require("@/db/models/subscription");
const currency = require("currency.js");

dotenv.config()

exports.create = async (req, res) => {
  // Our register logic starts here
  try {
    // Get group input
    const { courseId, name,price, start_date, end_date, schedule, length, start_time,type, end_time, status, teacherId} = req.body;
    // Validate group input
    if (!(courseId && name && teacherId)) {
      return res.status(400).json({ code: 400, message: 'All input is required' });
    }

    const value = {
      courseId: courseId,
      name: name,
      price: currency(price),
      start_date: start_date||getCurrentDateTime(),
      end_date: end_date||getCurrentDateTime(),
      schedule: schedule,
      length: length,
      type:type,
      start_time: start_time||getCurrentDateTime(),
      end_time: end_time||getCurrentDateTime(),
      teacherId: teacherId,
      status:status||'active',
      created_at: getCurrentDateTime(),
      updated_at: getCurrentDateTime()
    };
    const group = new Group(value);
    // validation
    var error = group.validateSync();
    if (error) {
      return res.status(409).json({ code: 409, message: 'Validatioan error', error: error });
    }
    await group.save();
    
    return res.status(201).json({status:"ok",message:"Group create successfully"});
  } catch (err) {
    console.log(err)
    return res.status(500).json({ code: 500, message: 'Internal server error', error: error });
  }
  // Our register logic ends here
}

exports.getList = async (req, res) => {

  // Our login logic starts here
  try {
    // Get user input
    var { pageNumber, pageSize,query } = req.query;
    pageNumber = parseInt(pageNumber);
    pageSize = parseInt(pageSize);
    // Validate user input
    if (!(pageNumber && pageSize)) {
      return res.status(400).send("Required inputs is missed!");
    }
    // Validate if user exist in our database
    var group = await Group.find({
      $or: [
        { name: new RegExp(query, 'i') }
      ]
    },['-__v'])
      .populate({
        path: 'courseId',
        select: '-_id name'
      })
      .populate({
        path: 'teacherId',
        select: '-_id first_name last_name middle_name'
      })
      .sort([['updated_at', -1]])
      .skip((pageNumber - 1) * pageSize)
      .limit(pageSize)
      .exec();

      const length = await Group.count({
        $or: [
          { name: new RegExp(query, 'i') }
        ]
      }).exec();

      const total_page=Math.floor(length/pageSize)+1

      if (group.err || group <= 0) {
        return res
          .status(404)
          .json({
            code: 404,
            message: "There as not any group yet"
          });
      } else {
        return res
          .status(200)
          .json({ code: 200, message: "group exist", total_page: total_page, group: group });
      }
  } catch (err) {
    userLogger.error(err);
    console.log(err);
    return res.status(500).json({ code: 500, message: 'Internal server error', error: err });
  }
  // Our register logic ends here
}

exports.getById = async (req, res) => {

  // Our login logic starts here
  try {
    // Get user input
    var { id } = req.query;
    // Validate if user exist in our database
    var group = await Group.find({_id:id}).populate('courseId teacherId')
      .sort([['updated_at', -1]])
      .exec();

      if (group.err || group <= 0) {
        return res
          .status(404)
          .json({
            code: 404,
            message: "There as not any group yet"
          });
      } else {
        return res
          .status(200)
          .json({ code: 200, message: "group exist", group: group });
      }
  } catch (err) {
    userLogger.error(err);
    console.log(err);
    return res.status(500).json({ code: 500, message: 'Internal server error', error: err });
  }
  // Our register logic ends here
}

exports.getByCourseId = async (req, res) => {

  // Our login logic starts here
  try {
    // Get user input
    var { courseId } = req.query;
    // Validate if user exist in our database
    const group = await Group.aggregate([
      { $match: { courseId: mongoose.Types.ObjectId(courseId) } }, // Match documents with the given courseId
      { $lookup: { 
          from: 'students', 
          localField: 'studentId', 
          foreignField: '_id', 
          as: 'studentDetails' 
      }},
      { $group: { 
          _id: '$courseId', 
          groups: { $push: '$$ROOT' }
      }},
      { $sort: { 'groups.updated_at': -1 } } // This sort might not work as expected; consider sorting within the groups
    ]).exec();

      if (group.err || group <= 0) {
        return res
          .status(404)
          .json({
            code: 404,
            message: "There as not any group yet"
          });
      } else {
        return res
          .status(200)
          .json({ code: 200, message: "group exist", group: group });
      }
  } catch (err) {
    userLogger.error(err);
    console.log(err);
    return res.status(500).json({ code: 500, message: 'Internal server error', error: err });
  }
  // Our register logic ends here
}

exports.getByGroupId = async (req, res) => {

  // Our login logic starts here
  try {
    // Get user input
    var { groupId } = req.query;
    // Validate if user exist in our database
    const students = await group_list.aggregate([
      // Match documents with the given groupId
      { $match: { groupId: mongoose.Types.ObjectId(groupId) } }, 
      // Lookup the details of the group from the Group model
      { $lookup: {
          from: 'groups', // Name of the Group collection
          localField: 'groupId', 
          foreignField: '_id', 
          as: 'groupDetails'
      }},
      // Unwind the groupDetails array to de-normalize the data
      { $unwind: '$groupDetails' },
      // Lookup student details
      { $lookup: { 
          from: 'students', 
          localField: 'studentId', 
          foreignField: '_id', 
          as: 'studentDetails'
      }},
      // Group by the groupId and collect the students
      { $group: { 
          _id: '$groupId', 
          students: { $push: '$$ROOT' },
          groupDetails: { $first: '$groupDetails' } // Include group details in the result
      }},
      // Sort by the updated_at field of the group
      { $sort: { 'groupDetails.updated_at': -1 } }
    ]).exec();
       

      if (students.err || students <= 0) {
        return res
          .status(404)
          .json({
            code: 404,
            message: "There as not any group yet"
          });
      } else {
        return res
          .status(200)
          .json({ code: 200, message: "group exist", students: students });
      }
  } catch (err) {
    userLogger.error(err);
    console.log(err);
    return res.status(500).json({ code: 500, message: 'Internal server error', error: err });
  }
  // Our register logic ends here
}
exports.getByGroupIdV2 = async (req, res) => {

  // Our login logic starts here
  try {
    // Get user input
    var { groupId } = req.query;
    // Validate if user exist in our database
    const students = await Subscription.aggregate([
      // Match documents with the given groupId
      { $match: { groupId: mongoose.Types.ObjectId(groupId) } },
      
      // Sort by student_id and updated_at to get the most recent subscription per student
      { $sort: { student_id: 1, updated_at: -1 } },
      
      // Group by student_id to calculate the totalAmount, first start_date, and last end_date
      { $group: { 
          _id: '$student_id', // Group by student_id to ensure uniqueness
          totalAmount: { $sum: '$amount' }, // Sum of all payments for the student
          amount: { $first: '$amount' }, // Keep the last payment (based on sorting)
          start_date: { $min: '$start_date' }, // Earliest start date
          end_date: { $max: '$end_date' }, // Latest end date
          comment: { $first: '$comment' },
          method: { $first: '$method' },
          discount: { $first: '$discount' },
          status: { $first: '$status' },
          created_at: { $first: '$created_at' },
          updated_at: { $first: '$updated_at' },
          groupId: { $first: '$groupId' } // Retain groupId for context
      }},
      
      // Lookup the student details from the Student model using student_id
      { $lookup: {
          from: 'students', // Name of the Student collection
          localField: '_id', // Use student_id for lookup (it's now the _id after grouping)
          foreignField: '_id', 
          as: 'studentDetails'
      }},
      
      // Unwind the studentDetails array to de-normalize the data
      { $unwind: '$studentDetails' },
      
      // Project the required fields
      { $project: {
          _id: 0, // Exclude the _id (which is the student_id here)
          studentDetails: {
            student_id: '$_id', // Project the student ID (comes from the _id after grouping)
            first_name: '$studentDetails.first_name',
            last_name: '$studentDetails.last_name',
            middle_name: '$studentDetails.middle_name'
          },
          totalAmount: 1, // Include the summed total amount of payments
          amount: 1, // Include the latest amount
          start_date: 1, // Include the earliest start date
          end_date: 1, // Include the latest end date
          comment: 1,
          method: 1,
          discount: 1,
          status: 1,
          created_at: 1,
          updated_at: 1
      }}
    ]).exec();
       

      if (students.err || students <= 0) {
        return res
          .status(404)
          .json({
            code: 404,
            message: "There as not any group yet"
          });
      } else {
        return res
          .status(200)
          .json({ code: 200, message: "group exist", students: students });
      }
  } catch (err) {
    userLogger.error(err);
    console.log(err);
    return res.status(500).json({ code: 500, message: 'Internal server error', error: err });
  }
  // Our register logic ends here
}

exports.deleteById = async (req, res) => {

  // Our login logic starts here
  try {
    // Get user input
    var { id } = req.query;
    // Validate if user exist in our database
    var group = await Group.findOneAndDelete({_id:id})
      .exec();
      if (group == null) {
        return res
          .status(404)
          .json({
            code: 404,
            message: "There as not any group yet"
          });
      } else {
        return res
          .status(200)
          .json({ code: 200, message: "group exist and deleted"});
      }
  } catch (err) {
    userLogger.error(err);
    console.log(err);
    return res.status(500).json({ code: 500, message: 'Internal server error', error: err });
  }
  // Our register logic ends here
}

exports.addStudent = async (req, res) => {
  // Our register logic starts here
  try {
    // Get group input
    const { studentId, groupId, discount=0 } = req.body;
    // Validate group input
    if (!(studentId && groupId)) {
      return res.status(400).json({ code: 400, message: 'All input is required' });
    }

    const value = {
      studentId: studentId,
      groupId: groupId
    };
    //check student already in this group
    // const check = await group_list(value);
    // if (check) {
    //   return res.status(400).json({ code: 400, message: 'Already is added to group' });
    // }

    const check = await group_list.find(value)
    console.log(check.length!=0);
    
    if (check.length!=0) {
      return res.status(409).json({ code: 409, message: 'Group already added', error: error });
    }
    
    
    const group = new group_list(value);
    // validation
    var error = group.validateSync();
    if (error) {
      return res.status(409).json({ code: 409, message: 'Validatioan error', error: error });
    }

    const courseId = await Group.find({_id:groupId}).select('courseId');
    if (!courseId) {
      return res.status(409).json({ code: 409, message: 'Validatioan error courseId', error: error });
    }

    await enrollStudentToSubscription(studentId,courseId[0].courseId,groupId,discount);    

    await group.save();    
    
    return res.status(201).json({status:"ok",message:"Student saved to group"});
  } catch (err) {
    console.log(err)
    return res.status(500).json({ code: 500, message: 'Internal server error', error: error });
  }
  // Our register logic ends here
}

exports.getByStudent = async (req, res) => {
  // Our register logic starts here
  try {
    // Get group input
    const { studentId } = req.query;
    // Validate group input
    if (!(studentId)) {
      return res.status(400).json({ code: 400, message: 'All input is required' });
    }
    const value = {
      studentId: studentId
    };
    const group = await group_list.find(value)
    // .populate({
    //   path: 'course_id',
    //   select: '-_id name'
    // })
    .populate({
      path: 'groupId',
      select: '-_id name'
    });
    // validation
    if (!group||group.length<=0) {
      return res.status(409).json({ code: 409, message: 'Not found'});
    }

    return res.status(201).json({ code: 201, message: 'Success', group: group });
  } catch (err) {
    console.log(err)
    return res.status(500).json({ code: 500, message: 'Internal server error', error: err });
  }
  // Our register logic ends here
}

exports.changeStudentGroupStatus = async (req, res) => {
  // Our register logic starts here
  try {
    // Get group input
    const { studentId, groupId, status } = req.body;
    // Validate group input
    if (!(studentId)) {
      return res.status(400).json({ code: 400, message: 'All input is required' });
    }
    const value = {
      student_id: studentId,
      _id: groupId
    };
    const group = await Subscription.findOne(value);
    // validation
    if (!group||group.length<=0) {
      return res.status(409).json({ code: 409, message: 'Not found'});
    }
    group.status=status
    await group.save()
    return res.status(201).json({ code: 201, message: 'Success group status succes fully changed'});
  } catch (err) {
    console.log(err)
    return res.status(500).json({ code: 500, message: 'Internal server error', error: err });
  }
  // Our register logic ends here
}

exports.changeGroupStatus = async (req, res) => {
  // Our register logic starts here
  try {
    // Get group input
    const { groupId, status } = req.body;
    
    const value = {
      _id: groupId
    };
    const group = await Group.findOne(value);
    // validation
    if (!group||group.length<=0) {
      return res.status(409).json({ code: 409, message: 'Not found'});
    }
    group.status=status
    await group.save()
    return res.status(201).json({ code: 201, message: 'Success group status succes fully changed'});
  } catch (err) {
    console.log(err)
    return res.status(500).json({ code: 500, message: 'Internal server error', error: err });
  }
  // Our register logic ends here
}
function getActiveGroupSchedules(groups) {
  const today = new Date();

  return groups
    .filter(group => group.status === 'active' && new Date(group.end_date) >= today)
    .map(group => {
      const schedule = group.schedule.map(day => ({
        dayOfWeek: day.toLowerCase(),
        date: getDateForNextDayOfWeek(day.toLowerCase(), new Date(group.start_date))
      }));

      return {
        title: group.name,
        schedule: schedule,
        start_time: group.start_time,
        end_time: group.end_time,
        start: new Date(group.start_date),
        end: new Date(group.end_date)
      };
    });
}

// Function to get the next date for a given day of the week
function getDateForNextDayOfWeek(dayOfWeek, startDate) {
  const dayIndex = ['sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday'].indexOf(dayOfWeek);
  const currentDate = new Date(startDate);
  const diff = dayIndex - currentDate.getDay() + (dayIndex <= currentDate.getDay() ? 7 : 0);
  currentDate.setDate(currentDate.getDate() + diff);
  return currentDate;
}

exports.getByWeek = async (req, res) => {
  // Our register logic starts here
  try {
    const groups = await Group.find()
    .populate({
      path: 'courseId',
      select: '-_id name'
    });
    // validation
    if (!groups||groups.length<=0) {
      return res.status(409).json({ code: 409, message: 'Not found'});
    }

    // for (const group of groups){
    //   console.log(group?.start_date);
    // }
    return res.status(201).json({ code: 201, message: 'Success', schedule: getActiveGroupSchedules(groups) });
  } catch (err) {
    console.log(err)
    return res.status(500).json({ code: 500, message: 'Internal server error', error: err });
  }
  // Our register logic ends here
}

//   const { refreshToken: requestToken } = req.body;

//   if (requestToken == null) {
//     return res.status(403).json({ message: "Refresh Token is required!" });
//   }

//   try {
//     let refreshToken = await RefreshToken.findOne({ token: requestToken });

//     if (!refreshToken) {
//       res.status(403).json({ message: "Refresh token is not in database!" });
//       return;
//     }

//     if (RefreshToken.verifyExpiration(refreshToken)) {
//       RefreshToken.findByIdAndRemove(refreshToken._id, { useFindAndModify: false }).exec();
      
//       res.status(403).json({
//         message: "Refresh token was expired. Please make a new signin request",
//       });
//       return;
//     }

//     let newAccessToken = jwt.sign({ id: refreshToken.user._id }, config.secret, {
//       expiresIn: config.jwtExpiration,
//     });

//     return res.status(200).json({
//       accessToken: newAccessToken,
//       refreshToken: refreshToken.token,
//     });
//   } catch (err) {
//     return res.status(500).send({ message: err });
//   }
// };