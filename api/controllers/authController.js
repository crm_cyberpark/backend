const config = require("@/config/auth.config");
const User = require("@models/user");
const RefreshToken = require("@models/refreshToken.model");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");
const dotenv = require('dotenv');
const { userLogger } = require("../helpers/logger");

dotenv.config()

exports.signup = async (req, res) => {

  // Our register logic starts here
  try {
    // Get user input
    const { phone, first_name, last_name, father_name, birth_date, gender, note, password, role } = req.body;
    // Validate user input
    if (!(phone && first_name && birth_date && gender)) {
      return res.status(400).json({ code: 400, message: 'All input is required' });
    }

    // check if user already exist
    // Validate if user exist in our database
    const oldUser = await User.findOne({ phone });

    if (oldUser) {
      return res.status(400).json({ code: 400, message: 'User Already Exist. Please Login' });
      // return res.status(409).send("User Already Exist. Please Login");
    }
    
    const value = {
      phone: phone,
      first_name: first_name,
      last_name: last_name,
      father_name: father_name,
      birth_date: birth_date,
      gender: gender, // sanitize: convert email to lowercase
      note: note,
      password: password,
      role: role || 'admin',
    };
    //Encrypt user password
    if (password) {
      encryptedPassword = await bcrypt.hash(password, 10);
      value.password=encryptedPassword
    }
    const user = new User(value);
    // validation
    var error = user.validateSync();
    if (error) {
      return res.status(409).json({ code: 409, message: 'Validatioan error', error: error });
      // return res.status(409).send("Validatioan error");
    }
    const saved_user = await user.save();
    // if (name && url) {
    //   user.img = { name: name, url: url }
    // }
    // return new user
    return res.status(201).json(saved_user);
  } catch (err) {
    console.log(err)
    return res.status(500).json({ code: 500, message: 'Internal server error', error: error });
  }
  // Our register logic ends here
};

exports.signin = async (req, res) => {
  // Our login logic starts here
  try {
    // Get user input
    const { phone, password } = req.body;
    // Validate user input
    if (!(phone && password)) {
      return res.status(400).send("All input is required");
    }
    // Validate if user exist in our database
    const user = await User.findOne({ phone: phone });

    if (user && (await bcrypt.compare(password, user.password))) {
      // Create token
      const token = jwt.sign({ id: user._id }, config.secret, {
        expiresIn: config.jwtExpiration,
      });
      let refreshToken = await RefreshToken.createToken(user);
      let authorities = [];
      for (let i = 0; i < user.role.length; i++) {
        authorities.push("ROLE_" + user.role[i].toUpperCase());
      }
      // save user token
      user.token = token;
      user.refreshToken = refreshToken;
      user.authorities = authorities;

      // user
      return res.status(200).json({code: 200,message: 'user exist', data: user, token: token, refreshToken: refreshToken, authorities: authorities });
    }
    return res.status(400).json({ code: 400, message: 'user does not exist and not verified' });
  } catch (err) {
    userLogger.error(err);
    console.log(err);
    return res.status(500).json({ code: 500, message: 'Internal server error', error: err });
  }
  // Our register logic ends here
}

exports.getById = async (req, res) => {

  // Our login logic starts here
  try {
    // Get user input
    var { id } = req.query;
    // Validate if user exist in our database
    var user = await User.find({_id:id})
      .sort([['updatedAt', -1]])
      .exec();

      if (user.err || user <= 0) {
        return res
          .status(404)
          .json({
            code: 404,
            message: "There as not any user yet"
          });
      } else {
        return res
          .status(200)
          .json({ code: 200, message: "user exist", user: user });
      }
  } catch (err) {
    userLogger.error(err);
    console.log(err);
    return res.status(500).json({ code: 500, message: 'Internal server error', error: err });
  }
  // Our register logic ends here
}

exports.deleteById = async (req, res) => {

  // Our login logic starts here
  try {
    // Get user input
    var { id } = req.query;
    // Validate if user exist in our database
    var user = await User.findOneAndDelete({_id:id})
      .exec();
      if (user==null) {
        return res
          .status(404)
          .json({
            code: 404,
            message: "There as not any user yet"
          });
      } else {
        return res
          .status(200)
          .json({ code: 200, message: "user exist and deleted"});
      }
  } catch (err) {
    userLogger.error(err);
    console.log(err);
    return res.status(500).json({ code: 500, message: 'Internal server error', error: err });
  }
  // Our register logic ends here
}

exports.getByToken = async (req, res) => {
  // Our login logic starts here
  try {
    // Get user input
    var id  = req.userId;
    // Validate if user exist in our database
    var user = await User.find({_id:id})
      .sort([['updatedAt', -1]])
      .exec();
      
      if (user.err || user <= 0) {
        return res
          .status(404)
          .json({
            code: 404,
            message: "There as not any user yet"
          });
      } else {
        return res
          .status(200)
          .json({ code: 200, message: "user exist", user: user });
      }
  } catch (err) {
    userLogger.error(err);
    console.log(err);
    return res.status(500).json({ code: 500, message: 'Internal server error', error: err });
  }
  // Our register logic ends here
}
// exports.refreshToken = async (req, res) => {
//   const { refreshToken: requestToken } = req.body;

//   if (requestToken == null) {
//     return res.status(403).json({ message: "Refresh Token is required!" });
//   }

//   try {
//     let refreshToken = await RefreshToken.findOne({ token: requestToken });

//     if (!refreshToken) {
//       res.status(403).json({ message: "Refresh token is not in database!" });
//       return;
//     }

//     if (RefreshToken.verifyExpiration(refreshToken)) {
//       RefreshToken.findByIdAndRemove(refreshToken._id, { useFindAndModify: false }).exec();
      
//       res.status(403).json({
//         message: "Refresh token was expired. Please make a new signin request",
//       });
//       return;
//     }

//     let newAccessToken = jwt.sign({ id: refreshToken.user._id }, config.secret, {
//       expiresIn: config.jwtExpiration,
//     });

//     return res.status(200).json({
//       accessToken: newAccessToken,
//       refreshToken: refreshToken.token,
//     });
//   } catch (err) {
//     return res.status(500).send({ message: err });
//   }
// };