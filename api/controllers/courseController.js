const Course = require("@/db/models/course");
const dotenv = require('dotenv');
const getCurrentDateTime = require("@/api/helpers/time");
const { userLogger } = require("../helpers/logger");

dotenv.config()
exports.create = async (req, res) => {
  // Our register logic starts here
  try {
    // Get group input
    const { name, comment, created_by } = req.body;
    // Validate group input
    if (!name) {
      return res.status(400).json({ code: 400, message: 'All input is required' });
    }
    // check if group already exist
    // Validate if group exist in our database
    const oldGroup = await Course.findOne({ name });
    if (oldGroup) {
      return res.status(400).json({ code: 400, message: 'Group Already Exist. Please do not create this group agian' });
      // return res.status(409).send("Group Already Exist. Please Login");
    }
    
    const value = {
      name: name,
      comment: comment,
      created_by: created_by,
      created_at: getCurrentDateTime(),
      updated_at: getCurrentDateTime()
    };
    const course = new Course(value);
    // validation
    var error = course.validateSync();
    if (error) {
      return res.status(409).json({ code: 409, message: 'Validatioan error', error: error });
    }
    await course.save();
    
    return res.status(201).json({status:"ok",message:"course is saved"});
  } catch (err) {
    console.log(err)
    return res.status(500).json({ code: 500, message: 'Internal server error', error: error });
  }
  // Our register logic ends here
};

exports.get_list = async (req, res) => {

  // Our login logic starts here
  try {
    // Get user input
    var { pageNumber, pageSize, query } = req.query;
    pageNumber = parseInt(pageNumber);
    pageSize = parseInt(pageSize);
    // Validate user input
    if (!(pageNumber && pageSize)) {
      return res.status(400).send("Required inputs is missed!");
    }
    // Validate if user exist in our database
    var course = await Course.find({
        $or: [
          { name: new RegExp(query, 'i') }
        ]
      })
      .sort([['updated_at', -1]])
      .skip((pageNumber - 1) * pageSize)
      .limit(pageSize)
      .exec();
    
      const length = await Course.find({
        $or: [
          { name: new RegExp(query, 'i') }
        ]
      })
      .exec();
      const total_page=Math.floor(length/pageSize)+1

      if (course.err || course <= 0) {
        return res
          .status(404)
          .json({
            code: 404,
            message: "There as not any course yet"
          });
      } else {
        return res
          .status(200)
          .json({ code: 200, message: "course exist",total_page:total_page, course: course });
      }
  } catch (err) {
    userLogger.error(err);
    console.log(err);
    return res.status(500).json({ code: 500, message: 'Internal server error', error: err });
  }
  // Our register logic ends here
}

exports.get_by_id = async (req, res) => {

  // Our login logic starts here
  try {
    // Get user input
    var { id } = req.query;
    // Validate if user exist in our database
    var course = await Course.find({_id:id})
      .sort([['updated_at', -1]])
      .exec();

      if (course.err || course <= 0) {
        return res
          .status(404)
          .json({
            code: 404,
            message: "There as not any course yet"
          });
      } else {
        return res
          .status(200)
          .json({ code: 200, message: "course exist", course: course });
      }
  } catch (err) {
    userLogger.error(err);
    console.log(err);
    return res.status(500).json({ code: 500, message: 'Internal server error', error: err });
  }
  // Our register logic ends here
}

exports.deleteById = async (req, res) => {

  // Our login logic starts here
  try {
    // Get user input
    var { id } = req.query;
    // Validate if user exist in our database
    var course = await Course.findOneAndDelete({_id:id})
      .exec();
      if (course == null) {
        return res
          .status(404)
          .json({
            code: 404,
            message: "There as not any course yet"
          });
      } else {
        return res
          .status(200)
          .json({ code: 200, message: "course exist and deleted"});
      }
  } catch (err) {
    userLogger.error(err);
    console.log(err);
    return res.status(500).json({ code: 500, message: 'Internal server error', error: err });
  }
  // Our register logic ends here
}

//   const { refreshToken: requestToken } = req.body;

//   if (requestToken == null) {
//     return res.status(403).json({ message: "Refresh Token is required!" });
//   }

//   try {
//     let refreshToken = await RefreshToken.findOne({ token: requestToken });

//     if (!refreshToken) {
//       res.status(403).json({ message: "Refresh token is not in database!" });
//       return;
//     }

//     if (RefreshToken.verifyExpiration(refreshToken)) {
//       RefreshToken.findByIdAndRemove(refreshToken._id, { useFindAndModify: false }).exec();
      
//       res.status(403).json({
//         message: "Refresh token was expired. Please make a new signin request",
//       });
//       return;
//     }

//     let newAccessToken = jwt.sign({ id: refreshToken.user._id }, config.secret, {
//       expiresIn: config.jwtExpiration,
//     });

//     return res.status(200).json({
//       accessToken: newAccessToken,
//       refreshToken: refreshToken.token,
//     });
//   } catch (err) {
//     return res.status(500).send({ message: err });
//   }
// };