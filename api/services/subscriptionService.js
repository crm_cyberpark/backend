// services/subscriptionService.js
const Subscription = require('@models/subscription');
const Course = require('@models/course');
const moment = require('moment');
const { transferMoney } = require('@controllers/paymentController');
const currency = require('currency.js');
const dotenv = require('dotenv');
const getCurrentDateTime = require('../helpers/time');
const payment_journal = require('@/db/models/payment_journal');
dotenv.config()

// Retrieve subscriptions ending today
async function getSubscriptionsEndingToday(day) {
    // Ensure the provided day is handled in UTC
    const startOfDay = moment.utc(day).startOf('day').toDate();
    const endOfDay = moment.utc(day).endOf('day').toDate();

    // console.log(`Querying subscriptions ending between ${startOfDay.toISOString()} and ${endOfDay.toISOString()}`);

    return await Subscription.find({
        end_date: {
            $gte: startOfDay,
            $lt: endOfDay
        },
        status: 'active'
    });

    // console.log(`Found ${subscriptions.length} subscriptions ending today`);
    // return subscriptions;
}
// Dummy payment processing function
async function processPayment(studentId, amount) {
    // Implement your payment gateway logic here
    // Return true if payment is successful, otherwise false
    try {
        await transferMoney(studentId, currency(amount), process.env.ADMIN_ID_KEY, 'System', 'Automatic pay for Course', 'bySystem')
        console.log('payment Success processPayment')
    }
    catch {
        console.log('payment error processPayment')
        return false;
    }
    return true; // Assume payment is successful for demonstration
}

// Extend subscription by one month or until the end date of the course
async function extendSubscription(subscription, courseEndDate, end_date, type = '') {
    const currentDate = moment().startOf('day');
    const remainingDays = moment(end_date).diff(currentDate, 'days');

    if (type === 'express') {
        subscription.end_date = end_date
    } else if (remainingDays >= 30) {
        // If remaining duration is more than a month, extend subscription by one month
        subscription.end_date = moment(currentDate).add(1, 'month').toDate();
    } else {
        // If remaining duration is less than a month, extend subscription until the end date of the course
        subscription.end_date = end_date
    }
    await subscription.save();
}

// Notify student (dummy function)
async function notifyStudent(studentId, message) {
    // Implement your notification logic here (e.g., email, SMS)
    console.log(`Notification to student ${studentId}: ${message}`);
}

// Calculate payment amount based on remaining days and course price
function calculatePayment(coursePrice, daysRemaining) {
    const pricePerDay = currency(coursePrice).divide(30); // Assuming 30 days per month
    return currency(pricePerDay).multiply(daysRemaining);
}

// Enroll student to subscription scheme
async function enrollStudentToSubscription(studentId, courseId, groupId, discount) {
    try {
        const course = await Course.findById(courseId);
        if (!course) {
            throw new Error('Course not found.');
        }

        const subscription = await Subscription.find({
            student_id: studentId,
            course_id: courseId,
            groupId: groupId
        });
        console.log('!subscription');
        console.log(subscription);
        if (!subscription) {
            throw new Error('subscription already exist.');
        }

        const today = moment().startOf('day').toDate();

        // Create new subscription
        const newSubscription = new Subscription({
            student_id: studentId,
            course_id: courseId,
            groupId: groupId,
            start_date: today,
            end_date: today,
            discount: 0,
            amount: 0,
            status: 'active',
            method: 'Naqd',
            comment: 'First time',
            created_at: getCurrentDateTime(),
            updated_at: getCurrentDateTime()
        });

        await newSubscription.save();
        await notifyStudent(studentId, `You have been enrolled in the course and your initial subscription is valid until ${moment(today).format('YYYY-MM-DD')}.`);
    } catch (error) {
        console.log(error)
        throw new Error('error enrollStudentToSubscription');
    }
}


// // Enroll student to subscription scheme
// async function enrollStudentToSubscriptionPayment(studentId, courseId, groupId, amount, start_date, end_date, discount) {
//     try {
        
//         const course = await Course.findById(courseId);
//         if (!course) {
//             throw new Error('Course not found.');
//         }

//         // const subscription = await Subscription.find({
//         //     student_id: studentId,
//         //     course_id: courseId,
//         //     groupId: groupId
//         // });

//         // console.log('!subscription');
//         // console.log(subscription);
//         // if (!subscription) {
//         //     throw new Error('subscription already exist.');
//         // }

//         const today = moment().startOf('day').toDate();

//         // Create new subscription
//         const newSubscription = new Subscription({
//             student_id: studentId,
//             course_id: courseId,
//             groupId: groupId,
//             start_date: start_date,
//             end_date: end_date,
//             discount: discount,
//             amount: amount,
//             status: 'active',
//             created_at: getCurrentDateTime(),
//             updated_at: getCurrentDateTime()
//         });

//         await newSubscription.save();
//         await notifyStudent(studentId, `You have been enrolled in the course and your initial subscription is valid until ${moment(today).format('YYYY-MM-DD')}.`);
//     } catch (error) {
//         console.log(error)
//         throw new Error('error enrollStudentToSubscription');
//     }
// }

async function journalPayment(studentId, course_id, group_id, paymentAmount, paymentType) {
    try {
        const course = await Course.findById(course_id);
        if (!course) {
            throw new Error('Course not found.');
        }

        // Create new subscription
        const payment = new payment_journal({
            student_id: studentId,
            courseId: course_id,
            groupId: group_id,
            paymentAmount: currency(paymentAmount),
            paymentType: paymentType,
            created_at: getCurrentDateTime(),
            updated_at: getCurrentDateTime()
        });

        await payment.save();
    } catch (error) {
        console.log(error)
        throw new Error('error journalPayment');
    }
}

module.exports = {
    journalPayment,
    getSubscriptionsEndingToday,
    processPayment,
    extendSubscription,
    notifyStudent,
    enrollStudentToSubscription,
    calculatePayment
};
