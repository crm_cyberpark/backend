// subscriptionProcessor.js
const cron = require('node-cron');
const moment = require('moment');
const { getSubscriptionsEndingToday, processPayment, extendSubscription, notifyStudent, calculatePayment, journalPayment } = require('@/api/services/subscriptionService');
const Course = require('@models/course');
const Group = require('@/db/models/group');
const Student = require('@/db/models/student');
const getCurrentDateTime = require('../helpers/time');

function isSameDay(date1, date2) {
    // Create Date objects from the input strings
    const d1 = new Date(date1);
    const d2 = new Date(date2);

    // Compare the year, month, and day parts of both dates
    return d1.getFullYear() === d2.getFullYear() &&
        d1.getMonth() === d2.getMonth() &&
        d1.getDate() === d2.getDate();
}
// Schedule task to run every day at midnight
cron.schedule('0 */1 * * *', async () => {
    console.log('Running subscription processing task');
    const today = moment().startOf('day').toDate();
    // const today = moment().add(1, 'day').startOf('day').toDate();
    const subscriptions = await getSubscriptionsEndingToday(today);
    for (const subscription of subscriptions) {
        // console.log(subscription);
        const course = await Course.findById(subscription.course_id);
        if (!course) {
            subscription.status = 'inactive'
            await subscription.save()
            console.log(`Course for subscription ${subscription._id} is not available`);
            continue; // Skip processing for unavailable courses
        }
        const group = await Group.findOne({ _id: subscription.groupId, status: 'active' });
        // console.log('group.end_date < today');
        // console.log(group.end_date < today);
        // console.log(!group || group.end_date < today);
        if (group.start_date < today) {
            console.log('kurs hali boshlanmagan');
            continue; // Skip processing for unavailable courses
        }
        if (!group || group.end_date < today) {
            subscription.status = 'inactive'
            await subscription.save()
            console.log(`Group for subscription ${subscription._id} is not available`);
            continue; // Skip processing for unavailable courses
        }
        // console.log(group);
        // console.log('group.end_date < today');
        // console.log(group.end_date);
        // console.log(today);
        // console.log(getCurrentDateTime());
        // console.log(isSameDay(group.end_date, today));
        if (isSameDay(group.end_date, today)) {
            group.status = 'inactive'
            await group.save()
            console.log(`Group ${group._id} is stopped for last day`);
            continue;
        }

        //student_id
        const student = await Student.findById(subscription.student_id);
        if (!student || student.status !== 'active') {
            console.log(`student for subscription ${subscription._id} is not available`);
            continue; // Skip processing for unavailable courses
        }

        const currentDate = moment(subscription.end_date).startOf('day');
        const remainingDays = moment(group.end_date).diff(currentDate, 'days') + 1; // Add 1 to include end date

        let daysToExtend;
        let paymentAmount;

        if (group.type === 'express') {
            paymentAmount = group.price
        } else if (remainingDays >= 30) {
            // If remaining duration is more than a month, pay for and extend by one month
            daysToExtend = 30;

            paymentAmount = group.price
        } else {
            // If remaining duration is less than a month, pay for the remaining days
            daysToExtend = remainingDays;
            paymentAmount = calculatePayment(group.price, remainingDays);
        }
        const discount = subscription.discount === 0 ? 1 : subscription.discount / 100;
        const success = await processPayment(subscription.student_id, paymentAmount * (-1) * discount);
        const payment_journal = await journalPayment(subscription.student_id, course._id, group._id, paymentAmount, 'System');
        if (success) {
            if (group.type === 'express') {
                await extendSubscription(subscription, course.end_date, group.end_date, 'express');
                await notifyStudent(subscription.student_id, `Your subscription has been renewed for ${daysToExtend} days.`);
            } else {
                await extendSubscription(subscription, daysToExtend, group.end_date);
                await notifyStudent(subscription.student_id, `Your subscription has been renewed for ${daysToExtend} days.`);
            }
        } else {
            subscription.status = 'inactive';
            await subscription.save();
            await notifyStudent(subscription.student_id, "Payment failed. Your subscription has been deactivated.");
        }
    }
});
