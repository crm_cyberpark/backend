const express = require('express');
const router = express.Router();
const { signup, signin, getById, deleteById,getByToken } = require('@controllers/authController');
const { verifyToken } = require('../middleware/auth');


/**
 * @swagger
 * /api/v1/user/signup:
 *   post:
 *     description: Sing up new User to Server!
 *     tags:
 *       - User
 *     parameters:
 *       - name: data
 *         description: JSON object containing pageNumber and pageSize
 *         in: body
 *         required: true
 *         schema:
 *           type: object
 *           properties:
 *             first_name:
 *               description: First Name
 *               type: string
 *               example: Anvar
 *             last_name:
 *               description: Last Name
 *               type: string
 *               example: Narzullayev
 *             father_name:
 *               description: father name
 *               example: Narzullayevich
 *               type: string
 *             email:
 *               description: Email of user
 *               example: anavar.narwer@gmail.com
 *               type: string
 *             img:
 *               description: Images link
 *               example: http://localhost:8081/api/v1/api-docs/#/User/post_api_v1_user_list
 *               type: string
 *             phone:
 *               description: Phone number of user
 *               example: 9895632663
 *               type: string
 *             password:
 *               description: Passwrod of user
 *               example: 94Wqdw56qa#jsd
 *               type: string
 *             role:
 *               description: Role of user
 *               example: admin
 *               type: string
 *     responses:
 *       201:
 *         description: Created
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 message:
 *                   type: string
 *                   description: A success message
 *                 data:
 *                   type: object
 *                   description: Response data
 *       400:
 *         description: Bad Request
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 message:
 *                   type: string
 *                   description: An error message
 *       500:
 *         description: Internal Server Error
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 message:
 *                   type: string
 *                   description: An error message
 */
router.post("/signup", signup);

router.get("/get", getById);
/**
 * @swagger
 * /api/v1/user/signin:
 *   post:
 *     description: Sign in User to Server!
 *     tags:
 *       - User
 *     parameters:
 *       - name: data
 *         description: JSON object containing pageNumber and pageSize
 *         in: body
 *         required: true
 *         schema:
 *           type: object
 *           properties: 
 *             email:
 *               description: Email of user
 *               example: anavar.narwer@gmail.com
 *               type: string
 *             password:
 *               description: Passwrod of user
 *               example: 94Wqdw56qa#jsd
 *               type: string  
 *     responses:
 *       201:
 *         description: Successful log in
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 message:
 *                   type: string
 *                   description: A success message
 *                 data:
 *                   type: object
 *                   description: Response data
 *       400:
 *         description: Bad Request
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 message:
 *                   type: string
 *                   description: An error message
 *       500:
 *         description: Internal Server Error
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 message:
 *                   type: string
 *                   description: An error message
 */
router.post("/signin", signin);

// router.post("/get_list", list);

router.delete("/delete", verifyToken,  deleteById);

router.post("/me",verifyToken, getByToken);

module.exports = router;