const express = require('express');
const router = express.Router();
const { create, get_list, get_by_id, deleteById } = require('@/api/controllers/courseController');


router.post("/", get_list);

router.get("/", get_by_id);

router.post("/create", create);

router.delete("/delete", deleteById);

module.exports = router;