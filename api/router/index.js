const express = require("express");
const student = require('@/api/router/student')
const auth = require('@/api/router/auth')
const teacher = require('@/api/router/teacher')
const leads = require('@/api/router/leads')
const course = require('@/api/router/course')
const group = require('@/api/router/group')
const payment = require('@/api/router/payment')
const file_uploads = require('@/api/router/file_uploads');
const { verifyToken } = require('@/api/middleware/auth');



const router = express.Router();
router.use('/auth', auth);
router.use('/students', verifyToken, student);
router.use('/teachers', verifyToken, teacher);
router.use('/leads', verifyToken, leads);
router.use('/courses', verifyToken, course);
router.use('/groups', verifyToken, group);
router.use('/payment', verifyToken, payment);
router.use('/upload', file_uploads);

module.exports = router
