const express = require('express');
const { list, payment, getById, deleteById, subs_list: subscription_list, deleteById_subs, calcuMnthCrs, calcuMnthCrsSystem, calcGroupMoney } = require('@controllers/paymentController');
const router = express.Router();

router.get("/", list);

router.post("/add", payment);

router.get("/getById", getById);

router.delete("/delete", deleteById);

router.delete("/delete_subs", deleteById_subs);

router.get("/subscription", subscription_list);

router.get("/calculateMonth", calcuMnthCrs);

router.post("/calculateGroup", calcGroupMoney);

router.get("/calculateMonthSystem", calcuMnthCrsSystem);

module.exports = router;